package net.xelcore.rplib.itemstack;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemStackBuilder {

    private static ItemMeta im;
    private static org.bukkit.inventory.ItemStack is;

    public ItemStackBuilder(Material Material) {
        is = new org.bukkit.inventory.ItemStack(Material);
        im = is.getItemMeta();
    }

    public ItemStackBuilder setDisplayName(String Displayname) {
        im.setDisplayName(Displayname);
        return this;
    }

    public ItemStackBuilder setLore(List<String> Lore) {
        im.setLore(Lore);
        return this;
    }

    public ItemStackBuilder setDamage(int Damage) {
        is.setDurability((short) Damage);
        return this;
    }

    public ItemStackBuilder setAmount(int Amount) {
        is.setAmount(Amount);
        return this;
    }

    public ItemStackBuilder addEnchantment(Enchantment Enchantment, int level) {
        is.addUnsafeEnchantment(Enchantment, level);
        return this;
    }

    public ItemStackBuilder removeEnchantment(Enchantment Enchantment) {
        if (!is.getEnchantments().isEmpty())
            if (is.getEnchantments().keySet().contains(Enchantment))
                is.removeEnchantment(Enchantment);
        return this;
    }

    public ItemStackBuilder addItemFlag(ItemFlag Flag) {
        im.addItemFlags(Flag);
        return this;
    }

    public ItemStackBuilder removeItemFlag(ItemFlag Flag) {
        im.removeItemFlags(Flag);
        return this;
    }

    public ItemMeta getItemMeta() {
        return im;
    }

    public org.bukkit.inventory.ItemStack build() {
        is.setItemMeta(im);
        return is;
    }

}
