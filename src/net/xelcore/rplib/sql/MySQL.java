package net.xelcore.rplib.sql;

import net.xelcore.rplib.main.Main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQL {

    private Connection connection;
    private String hostname;
    private String database;
    private String username;
    private String password;
    private Integer port;

    public MySQL(String hostname, String database, String username, String password, Integer port) {
        this.hostname = hostname;
        this.database = database;
        this.username = username;
        this.password = password;
        this.port = port;
    }

    public Connection connect() {
        if(this.hostname != null) {
            if(this.database != null) {
                if(this.username != null) {
                    if(this.password != null) {
                        if(this.port != null) {
                            try {
                                this.connection = DriverManager.getConnection("jdbc:mysql://" + this.hostname + ":" + this.port + "/" + this.database + "?autoReconnect=true&useUnicode=yes&sslMode=DISABLED&useSSL=false", this.username, this.password);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            if(this.connection != null) {
                                Main.log.info("MySQL successfully connected to " + this.hostname + ":" + this.port);
                            } else {
                                Main.log.error("MySQL couldn't connect to " + this.hostname + ":" + this.port);
                            }
                        } else {
                            Main.log.error("MySQL port can't be null");
                        }
                    } else {
                        Main.log.error("MySQL password can't be null");
                    }
                } else {
                    Main.log.error("MySQL username can't be null");
                }
            } else {
                Main.log.error("MySQL database can't be null");
            }
        } else {
            Main.log.error("MySQL hostname can't be null");
        }
        return this.connection;
    }

    public void disconnect() throws SQLException {
        if(this.connection != null) {
            this.connection.close();
        } else {
            Main.log.error("MySQL cant disconnect while not connected");
        }
    }

    public String getHostname() {
        return hostname;
    }

    public String getDatabase() {
        return database;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Integer getPort() {
        return port;
    }
}
