package net.xelcore.rplib.inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import net.xelcore.rplib.itemstack.ItemStackBuilder;

@SuppressWarnings("unused")
public abstract class InventoryBuilder implements Listener {
    public static Plugin Instance;

    public static void setPlugin(Plugin arg0) {
        InventoryBuilder.Instance = arg0;
    }

    public static ArrayList<InventoryBuilder> openGUIs = new ArrayList<>();

    private void registerEvents() {
        if (Instance != null) {
            Bukkit.getPluginManager().registerEvents(this, Instance);
        } else {
            throw new NullPointerException("Instance has not been set");
        }
    }

    private int depth = 1;
    private HashMap<Integer, ItemStack> content = new HashMap<>();
    private org.bukkit.inventory.Inventory inv;
    private Player player;
    private Location invOpenLocation;
    private Object obj = null;
    // Only for close | open Inventory Event
    private boolean isClosed;
    private boolean ignoreClose = false;;
    private org.bukkit.inventory.Inventory openedGui;

    public InventoryBuilder(Player player, String title, int rows) {
        invOpenLocation = player.getLocation();

        int maxDepth = 0;
        for (InventoryBuilder gui : openGUIs) {
            if (gui.player.getUniqueId().equals(player.getUniqueId()) && gui.depth >= maxDepth) {
                maxDepth = gui.depth;
            }
        }
        depth = maxDepth + 1;

        openGUIs.add(this);
        this.player = player;
        registerEvents();

        if (rows > 6)
            rows = 6;
        inv = Bukkit.createInventory(player, 9 * rows, title);

        openInventory();
    }

    public InventoryBuilder(Player player, String title, int rows, Object obj) {
        invOpenLocation = player.getLocation();
        this.obj = obj;

        int maxDepth = 0;
        for (InventoryBuilder gui : openGUIs) {
            if (gui.player.getUniqueId().equals(player.getUniqueId()) && gui.depth >= maxDepth) {
                maxDepth = gui.depth;
            }
        }
        depth = maxDepth + 1;

        openGUIs.add(this);
        this.player = player;
        registerEvents();

        if (rows > 6)
            rows = 6;
        inv = Bukkit.createInventory(player, 9 * rows, title);

        openInventory();
    }

    public abstract void onLoad();

    public abstract void onItemClick(InventoryClickEvent e, ItemStack stack);

    public final void openInventory() {
        clearContent();
        onLoad();

        if (!content.isEmpty()) {
            for (int i : content.keySet()) {
                inv.setItem(i, content.get(i));
            }
        }

        player.openInventory(inv);
    }

    public final void closeInventory() {
        player.closeInventory();
    }

    public final boolean isOpenInventory() {
        if (player.getOpenInventory().getTopInventory().equals(inv))
            return true;
        return false;
    }

    public final ArrayList<Integer> getIndex(int page, int pages) {
        try {
            int steps = 25;
            int percentage = (int) Math.ceil((((double) 100) * (double) page) / (double) pages);

            if (pages < 4) {
                if (pages == 3) {
                    if (Math.round(percentage) == 33) {
                        return new ArrayList<>(Arrays.asList(1));
                    } else if (Math.round(percentage) == 67) {
                        return new ArrayList<>(Arrays.asList(2, 3));
                    } else if (Math.round(percentage) == 100) {
                        return new ArrayList<>(Arrays.asList(4));
                    }
                } else if (pages == 2) {
                    if (Math.round(percentage) == 50) {
                        return new ArrayList<>(Arrays.asList(1, 2));
                    } else if (Math.round(percentage) == 100) {
                        return new ArrayList<>(Arrays.asList(3, 4));
                    }
                } else if (pages == 1) {
                    return new ArrayList<>(Arrays.asList(1, 2, 3, 4));
                }

            } else {
                for (int i = 1; i <= 4; i++) {
                    if (percentage <= (i == 4 ? 100 : steps * i) && percentage >= (i - 1 == 0 ? 0 : steps * (i - 1))) {
                        return new ArrayList<>(Arrays.asList(i));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>(Arrays.asList(4));
    }

    public final Player getPlayer() {
        return player;
    }

    public final org.bukkit.inventory.Inventory getInventory() {
        return inv;
    }

    public final int getDepth() {
        return depth;
    }

    public final HashMap<Integer, ItemStack> getContent() {
        return content;
    }

    public final int getGapIndex(int slot) {
        if (inv.getSize() / 9 <= 2)
            return -1;

        if (slot >= 10 && slot <= 16)
            return slot - 10;
        if (slot >= 19 && slot <= 25)
            return slot - 12;
        if (slot >= 28 && slot <= 34)
            return slot - 14;
        if (slot >= 37 && slot <= 43)
            return slot - 16;

        return 0;
    }

    public final Object getObj() {
        return obj;
    }

    public final <T> T getObj(Class<T> clazz) {
        try {
            return clazz.cast(obj);
        } catch (ClassCastException e) {
            return null;
        }
    }

    public final void setObj(Object obj) {
        this.obj = obj;
    }

    public final void clearContent() {
        content.clear();
        inv.clear();
    }

    public final void fillInventory(ItemStack stack) {
        for (int x = 1; x <= inv.getSize() / 9; x++) {
            for (int y = 1; y <= 9; y++) {
                addItem(x, y, stack);
            }
        }
    }

    public final void fillFrame(ItemStack stack) {
        if (inv.getSize() / 9 <= 2) {
            fillInventory(stack);
            return;
        }

        for (int x = 1; x <= inv.getSize() / 9; x++) {
            for (int y = 1; y <= 9; y++) {
                if (x == 1 || x == inv.getSize() / 9) {
                    addItem(x, y, stack);
                } else if (y == 1 || y == 9) {
                    addItem(x, y, stack);
                }
            }
        }
    }

    public final void fillGaps(ArrayList<ItemStack> stacks, int startindex) {
        if (inv.getSize() / 9 <= 2)
            return;

        int row = 1;
        int colum = 1;

        for (int i = startindex; i < stacks.size(); i++) {
            if (i - startindex >= (inv.getSize() / 9 - 2) * 7)
                return;
            if (colum >= 8) {
                row++;
                colum = 1;
            }

            int slot = (row * 9) + colum;
            addItem(slot, stacks.get(i));
            colum++;
        }
    }

    public final void addItem(int row, int colum, ItemStack stack) {
        int slot = row == 1 ? 0 : (row - 1) * 9;
        slot += colum - 1;

        content.put(slot, stack);
    }

    public final void addItem(int slot, ItemStack stack) {
        content.put(slot, stack);
    }

    public final ItemStack getContentItem(int row, int colum) {
        int slot = row == 1 ? 0 : (row - 1) * 9;
        slot += colum - 1;

        return content.get(slot);
    }

    public final ItemStack getContentItem(int slot) {
        return content.get(slot);
    }

    public final boolean isTheSameItem(int row, int colum, ItemStack stack) {
        if (getContentItem(row, colum) == null)
            return false;
        return isTheSameItem(getContentItem(row, colum), stack);
    }

    public final boolean isTheSameItem(int slot, ItemStack stack) {
        if (getContentItem(slot) == null)
            return false;
        return isTheSameItem(getContentItem(slot), stack);
    }

    public final boolean isTheSameItem(ItemStack stack1, ItemStack stack2) {
        if (stack1 == null && stack2 == null)
            return false;
        if (stack1.getType() == Material.LEGACY_SKULL && stack2.getType() == Material.LEGACY_SKULL) {
            if (stack1.getDurability() == stack2.getDurability())
                if (stack1.getAmount() == stack2.getAmount())
                    if (stack1.getItemMeta().getDisplayName() == stack2.getItemMeta().getDisplayName())
                        return true;
            return false;
        } else {
            return stack1.equals(stack2);
        }
    }

    @EventHandler
    public final void invclick_event(InventoryClickEvent e) {
        if (e.getInventory() != null) {
            if (e.getCurrentItem() != null) {
                if (e.getInventory().equals(inv)) {
                    e.setCancelled(true);
                    onItemClick(e, e.getCurrentItem());
                }
            }
        }
    }

    @EventHandler
    public final void invclose_event(InventoryCloseEvent e) {
        if (e.getPlayer().getUniqueId().equals(player.getUniqueId())) {
            if (!ignoreClose)
                isClosed = true;

            Bukkit.getScheduler().scheduleSyncDelayedTask(Instance, new Runnable() {
                @Override
                public void run() {
                    if (isClosed) {
                        remove();
                        return;
                    }

                    if (InventoryBuilder.isInvGui(openedGui)) {
                        InventoryBuilder gui = (InventoryBuilder) InventoryBuilder.getGui((Player) e.getPlayer(), openedGui);
                        if (depth > gui.depth) {
                            remove();
                        }
                    }
                    ignoreClose = false;
                }
            }, 2);
        }
    }

    @EventHandler
    public final void invopen_event(InventoryOpenEvent e) {
        if (e.getPlayer().getUniqueId().equals(player.getUniqueId())) {
            if (isClosed) {
                isClosed = false;
                openedGui = (org.bukkit.inventory.Inventory) e.getInventory();
            }
        }
    }

    public final void ignoreClose() {
        this.ignoreClose = true;
    }

    @EventHandler
    public final void playerquit_event(PlayerQuitEvent e) {
        if (e.getPlayer().getUniqueId().equals(player.getUniqueId())) {
            remove();
        }
    }

    public final void remove() {

        if (isOpenInventory())
            player.closeInventory();
        openGUIs.remove(this);
        HandlerList.unregisterAll(this);
    }

    public static final InventoryBuilder getGui(Player p, org.bukkit.inventory.Inventory inv) {
        for (InventoryBuilder gui : openGUIs) {
            if (gui.player.getUniqueId().equals(p.getUniqueId()) && gui.inv.equals(inv))
                return gui;
        }
        return null;
    }

    public static final InventoryBuilder getGui(Player p, int depth) {
        for (InventoryBuilder gui : openGUIs) {
            if (gui.player.getUniqueId().equals(p.getUniqueId()) && gui.depth == depth)
                return gui;
        }
        return null;
    }

    public static final boolean isInvGui(org.bukkit.inventory.Inventory inv) {
        for (InventoryBuilder gui : openGUIs) {
            if (gui.getInventory().equals(inv))
                return true;
        }
        return false;
    }
}