package net.xelcore.rplib.command;

import net.xelcore.rplib.main.Main;
import net.xelcore.rplib.message.Message;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DebugCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            if(p.hasPermission("rp.lib.command.debug")) {
                if(!(args.length > 0)) {
                    Main.log.toggleBroadcast();
                } else {
                    new Message(p).wrongcmdusage("/debug");
                }
            } else {
                new Message(p).noperms();
            }
        } else {
            Main.log.toggleBroadcast();
        }
        return true;
    }
}
