package net.xelcore.rplib.time;

import java.text.SimpleDateFormat;

public class Time {

    private long format;

    public Time(long format) {
        this.format = format;
    }

    public String year(boolean century) {
        if(century) {
            return new SimpleDateFormat("yyyy").format(this.format);
        } else {
            return new SimpleDateFormat("yy").format(this.format);
        }
    }

    public String month() {
        return new SimpleDateFormat("MM").format(this.format);
    }

    public String day() {
        return new SimpleDateFormat("dd").format(this.format);
    }

    public String hour() {
        return new SimpleDateFormat("HH").format(this.format);
    }

    public String minute() {
        return new SimpleDateFormat("mm").format(this.format);
    }

    public String second() {
        return new SimpleDateFormat("ss").format(this.format);
    }

    public String millisecond() {
        return new SimpleDateFormat("SS").format(this.format);
    }

    public String date() {
        return new SimpleDateFormat("dd.MM.yyyy").format(this.format);
    }

    public String time(boolean millis) {
        if(millis) {
            return new SimpleDateFormat("HH:mm:ss:SS").format(this.format);
        } else {
            return new SimpleDateFormat("HH:mm:ss").format(this.format);
        }
    }

}
