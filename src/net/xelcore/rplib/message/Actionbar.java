package net.xelcore.rplib.message;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

public class Actionbar {
	
	public Actionbar(Player Player, String Message) {
		Player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(Message));
	}

}
