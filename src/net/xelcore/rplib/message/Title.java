package net.xelcore.rplib.message;

import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_15_R1.IChatBaseComponent;
import net.minecraft.server.v1_15_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_15_R1.PacketPlayOutTitle;
import net.minecraft.server.v1_15_R1.PacketPlayOutTitle.EnumTitleAction;

public class Title {

	public Title(Player Player, String Title, String Subtitle, int FadeinTitle, int StayTitle, int FadeoutTitle,
			int FadeinSubTitle, int StaySubTitle, int FadeoutSubTitle) {
		IChatBaseComponent icbot = ChatSerializer.a("{\"text\": \"" + Title + "\"}");
	    IChatBaseComponent icbost = ChatSerializer.a("{\"text\": \"" + Subtitle + "\"}");
	    PacketPlayOutTitle packet1 = new PacketPlayOutTitle(EnumTitleAction.TIMES, icbot, FadeinTitle, StayTitle, FadeoutTitle);
	    PacketPlayOutTitle packet2 = new PacketPlayOutTitle(EnumTitleAction.TIMES, icbost, FadeinSubTitle, StaySubTitle, FadeoutSubTitle);
	    PacketPlayOutTitle packet3 = new PacketPlayOutTitle(EnumTitleAction.TITLE, icbot);
	    PacketPlayOutTitle packet4 = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, icbost);
	    ((CraftPlayer)Player).getHandle().playerConnection.sendPacket(packet1);
	    ((CraftPlayer)Player).getHandle().playerConnection.sendPacket(packet2);
	    ((CraftPlayer)Player).getHandle().playerConnection.sendPacket(packet3);
	    ((CraftPlayer)Player).getHandle().playerConnection.sendPacket(packet4);

	}

}
