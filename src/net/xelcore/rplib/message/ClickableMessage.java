package net.xelcore.rplib.message;

import net.minecraft.server.v1_15_R1.IChatBaseComponent;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_15_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_15_R1.PacketPlayOutChat;

public class ClickableMessage {

	public ClickableMessage(Player Player, String Text, String ClickableText, String Command) {
		IChatBaseComponent icbo = ChatSerializer
				.a("\n{\"text\":\"" + Text + "\",\"extra\":" + "[{\"text\":\"" + ClickableText
						+ "\",\"clickEvent\":{\"action\":\"run_command\",\"value\":" + "\"/" + Command + "\"}}]}");
		PacketPlayOutChat packet = new PacketPlayOutChat(icbo);
		((CraftPlayer) Player).getHandle().playerConnection.sendPacket(packet);
	}

}
