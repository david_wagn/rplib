package net.xelcore.rplib.message;

import org.bukkit.entity.Player;

public class Message {

    private String prefix = "§3RP §8× §7";
    private String errorprefix = "§4§l! §8× §7";
    private Player player;

    public Message(Player p) {
        if(p.isOnline()) {
            this.player = p;
        } else {
            return;
        }
    }

    public void prefix(String message) {
        this.player.sendMessage(this.prefix + message);
    }

    public void errorprefix(String message) {
        this.player.sendMessage(this.errorprefix + message);
    }

    public void noperms() {
        this.player.sendMessage(this.errorprefix + "Du hast keine Berechtigung für diesen Befehl");
    }

    public void notonline() {
        this.player.sendMessage(this.errorprefix + "Dieser Spieler ist nicht online");
    }

    public void cmdnotfound(String command) {
        this.player.sendMessage(this.errorprefix + "Der Befehl \"" + command + "\" wurde nicht gefunden");
    }

    public void wrongcmdusage(String usage) {
        this.player.sendMessage(this.errorprefix + "Bitte verwende \"" + usage + "\"");
    }
}
