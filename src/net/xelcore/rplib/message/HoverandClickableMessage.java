package net.xelcore.rplib.message;

import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_15_R1.IChatBaseComponent;
import net.minecraft.server.v1_15_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_15_R1.PacketPlayOutChat;

public class HoverandClickableMessage {

	public HoverandClickableMessage(Player Player, String Text, String ClickableText, String Hovertext,
			String Command) {
		IChatBaseComponent icbo = ChatSerializer.a("{\"text\":\"" + Text + "\",\"extra\":" + "[{\"text\":\""
				+ ClickableText + "\",\"hoverEvent\":{\"action\":\"show_text\", " + "\"value\":\"" + Hovertext
				+ "\"},\"clickEvent\":{\"action\":\"run_command\",\"value\":" + "\"/" + Command + "\"}}]}");
		PacketPlayOutChat packet = new PacketPlayOutChat(icbo);
		((CraftPlayer) Player).getHandle().playerConnection.sendPacket(packet);
	}

}
