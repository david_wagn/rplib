package net.xelcore.rplib.message;

import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_15_R1.IChatBaseComponent;
import net.minecraft.server.v1_15_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_15_R1.PacketPlayOutChat;

public class HoverableMessage {

	public HoverableMessage(Player Player, String Text, String HoverableText, String HoverText) {
		IChatBaseComponent icbo = ChatSerializer
				.a("{\"text\":\"" + Text + "\",\"extra\":" + "[{\"text\":\"" + HoverableText
						+ "\",\"hoverEvent\":{\"action\":\"show_text\", " + "\"value\":\"" + HoverText + "\"}}]}");
		PacketPlayOutChat packet = new PacketPlayOutChat(icbo);
		((CraftPlayer) Player).getHandle().playerConnection.sendPacket(packet);
	}

}
