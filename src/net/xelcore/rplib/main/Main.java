package net.xelcore.rplib.main;

import net.xelcore.rplib.command.DebugCommand;
import net.xelcore.rplib.logger.Logger;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Logger log = new Logger();

    @Override
    public void onEnable() {
        this.getCommand("debug").setExecutor(new DebugCommand());
    }

    @Override
    public void onDisable() {
    }
}
