package net.xelcore.rplib.logger;

import org.bukkit.Bukkit;

import java.util.Random;

public class Logger {

    private boolean broadcast = false;

    public Logger() {}

    public void debug(String invoker, String message) {
        if(this.broadcast) {
            Bukkit.broadcastMessage("[§6@" + invoker + "§r] " + message);
        }
    }

    public String getCall() {
        Random random = new Random();
        int nextInt = random.nextInt(256*256*256);
        return String.format("#%06x", nextInt);
    }

    public void toggleBroadcast() {
        if(this.broadcast) {
            this.debug("rplib>Logger", "Disabled debug broadcast");
            this.broadcast = false;
        } else {
            this.broadcast = true;
            this.debug("rplib>Logger", "Enabled debug broadcast");
        }
    }

    public void info(String message) {
        if(this.broadcast) {
            Bukkit.broadcastMessage("§9" + message);
        }
        else Bukkit.getLogger().info(message);
    }

    public void warn(String message) {
        if(this.broadcast) Bukkit.broadcastMessage("§e" + message);
        else Bukkit.getLogger().warning(message);
    }

    public void error(String message) {
        if(this.broadcast) Bukkit.broadcastMessage("§4" + message);
        else Bukkit.getLogger().severe(message);
    }

}
