package net.xelcore.rplib.tablist;

import org.bukkit.entity.Player;

public class Tablist {

    private Player player;
    private String header;
    private String footer;

    public Tablist(Player player, String header, String footer) {
        this.player = player;
        this.header = header;
        this.footer = footer;
    }

    public void send() {
        this.player.setPlayerListHeaderFooter(this.header, this.footer);
    }

}
