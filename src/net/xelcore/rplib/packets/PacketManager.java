package net.xelcore.rplib.packets;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class PacketManager {

    private Player player;

    public PacketManager(Player p) {
        this.player = p;
    }

    public Class<?> getNMSClass(String name) {
        String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        try {
            return Class.forName("net.minecraft.server." + version + "." + name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void sendPacket(Object packet) {
        try {
            Object handle = this.player.getClass().getMethod("getHandle", new Class[0]).invoke(this.player, new Object[0]);
            Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
            playerConnection.getClass().getMethod("sendPacket", new Class[] { getNMSClass("Packet") })
                    .invoke(playerConnection, new Object[] { packet });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
