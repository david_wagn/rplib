package net.xelcore.rplib.packets;

import org.bukkit.entity.Player;

import java.lang.reflect.Field;

public class PlayerPing {

    private Player player;

    public PlayerPing(Player p) {
        this.player = p;
    }

    private Class<?> getNMSClass(String name) {
        String version = org.bukkit.Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        try {
            return Class.forName("org.bukkit.craftbukkit." + version + "." + name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int get() {
        try {
            Object o = getNMSClass("entity.CraftPlayer").cast(this.player);
            Object entityplayer = o.getClass().getMethod("getHandle", new Class[0]).invoke(o, new Object[0]);
            Field f = entityplayer.getClass().getField("ping");
            return f.getInt(entityplayer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

}
